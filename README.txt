
Copyright 2006 http://2bits.com

This module provides users with the ability to refer other users
to a site they are registered at. When the new users registers,
the referral is recorded.

Users can see a list of users they referred, and the site admin
can view more detailed reports.

Basic Functionality
-------------------
The module works by displaying a referral link (such as referral/123)
in the user's page. When a non-registered visitor clicks on this link,
and then registers to the site, the referring user's ID is recorded
in the database.

If the Adsense module is installed and Revenue sharing is enabled, then
the referral is used to share the revenue between the referring user and
the referred user.

Optionally, points are awarded for the referring user (requires 
userpoints module).

The link for referral for each user is displayed in "my account" menu.

Only active users are counted in referrals. Blocked users, or users
not yet approved by the administrator are not counted in referrals.

Reports
-------
On the "my account" page, there is a link to a report on users you have
referred.

There are also admin reports under Administer -> Logs.

Two reports are provided:
- Summary: Lists each user and how many users they referred, and the date
  of the last referral.
- Details: Lists each users, and each user they have referred, the date/time
  of referral, the IP address, and Referral link.

Flag/Unflagged
--------------
Flagging of referrals can be used as a means of tracking with any special
meaning attached to it. For example, if your site needs to pay the referring
user for the number of referrals they make, then there is an "unflagged"
report. You can see the outstanding number of referrals, pay the referring
user, and unflag the current referrals to make way for the next billing
cycle. Or you can simply use this as a way to see referrals since you
last checked, and flag them.

Installation
------------
To install this module, upload or copy the directory called "referral" to
your modules directory.

Configuration
-------------
To enable this module do the following:

1. Go to Administer -> Modules, and click enable. Make sure there are no error
   messages.

2. Go to Administer -> Access Control and enable for the roles you want.
   Users with 'admin referral' permission can view the detailed

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

Author
------
<a href="http://baheyeldin.com">Khalid Baheyeldin</a> of <a href="http://2bits.com">2bits.com</a>.

The author can also be contacted for paid customizations of this module as well as Drupal consulting,
installation, development, and customizations.
